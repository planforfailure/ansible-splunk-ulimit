# Ulimit Configuration

A standardised ulimit configuration defined in `/etc/systemd/system/Splunkd.service` for all hosts in `[servers]` host grouping, so when applications are upgraded they can be reverted back to their original settings should the upgrade process deviate from the standard settings.

The role uses [jinja2](https://docs.ansible.com/ansible/latest/user_guide/playbooks_templating.html) templating for dynamically setting `ansible_memtotal_mb` to set the [MemoryLimit](https://manpages.debian.org/testing/systemd/systemd.resource-control.5.en.html) in Megabytes _(with it currently set in bytes per host)_ ini each systemd file. It also uses variables  defined in `var/main.yml` for `LimitNOFILE & LimitNPROC`  as per below.

![alt text](images/ulimit_table.jpg "ulimit table")

### Components

- ulimit.yml 
   - Backup `/etc/systemd/system/Splunkd.service`.
   - Copy Jinja2 systemd service file with defined vars. 
   - Updates `/var/log/CHANGELOG`.
- [handlers/main.yml](handlers/main.yml) -  daemon-reload && Splunkd restart.
- [vars/main.yml](vars/main.yml) - defined variables.

#### Installation

The role requires escalated privileges on the target hosts:- 

 `$ /usr/bin/ansible-playbook -i inventory ulimit.yml -b -K`

 The log output can be found in `/tmp/ansible.log` on the control node.

### Summary
Upon successful completion and with no failures in the callback output, you can run the following to confirm the configuration changes were successfully applied and `Splunkd.service` restarted with no errors.

`$ ansible -i inventory all -m shell -a "systemctl is-active Splunkd"`

**NOTE:** When applying against Heavy forwarders, ensure you read [this](HF.md)
